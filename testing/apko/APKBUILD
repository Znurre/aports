# Maintainer: Ariadne Conill <ariadne@dereferenced.org>
pkgname=apko
pkgver=0.2.2
pkgrel=0
pkgdesc="declarative APK-based container building tool with support for Sigstore signatures"
url="https://github.com/chainguard-dev/apko"
arch="all"
license="Apache-2.0"
# Explicitly depend on apk-tools, so that this package can be used without alpine-base.
depends="apk-tools"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/chainguard-dev/apko/archive/v$pkgver/apko-$pkgver.tar.gz"

build() {
	mkdir build
	go build -o build/ -tags -tags=pivkey,pkcs11key "$builddir"/...

	for i in bash fish zsh; do
		"$builddir"/build/apko completion $i > "$builddir"/apko.$i
	done
}

check() {
	go test "$builddir"/...
}

package() {
	install -Dm755 "$builddir"/build/apko "$pkgdir"/usr/bin/apko

	install -Dm644 "$builddir"/apko.bash "$pkgdir"/usr/share/bash-completion/completions/apko
	install -Dm644 "$builddir"/apko.fish "$pkgdir"/usr/share/fish/completions/apko.fish
	install -Dm644 "$builddir"/apko.zsh "$pkgdir"/usr/share/zsh/site-functions/_apko
}

sha512sums="
7cb4fbc63ed6931b31c8af45ecde1d2458a994201bd5ed23a6083a2c02e33d1ed5203cdc42579dad5f6e83f9d12eb763d79ab8d95c3a102d0603d2b8aa4f865a  apko-0.2.2.tar.gz
"
