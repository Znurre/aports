# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=stellarium
pkgver=0.22.0
pkgrel=0
pkgdesc="A stellarium with great graphics and a nice database of sky-objects"
url="http://stellarium.org/"
arch="all !armhf" # Limited by qt5-qtmultimedia-dev
license="GPL-2.0-or-later"
makedepends="
	boost-dev
	cmake
	freetype-dev
	gpsd-dev
	libpng-dev
	mesa-dev
	openssl1.1-compat-dev
	qt5-qtcharts-dev
	qt5-qtlocation-dev
	qt5-qtmultimedia-dev
	qt5-qtscript-dev
	qt5-qtserialport-dev
	qt5-qttools-dev
"
subpackages="$pkgname-doc"
source="https://github.com/Stellarium/stellarium/releases/download/v$pkgver/stellarium-$pkgver.tar.gz"
[ "$CARCH" = "x86" ] && options="!check"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_TESTING=1
	cmake --build build
}

check() {
	cd build
	# Exclude a broken locale test
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'test(INDIConnection|TelescopeControl_INDI|TelescopeClientINDI|StelSkyCultureMgr)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
17cfc86f5b684d8e40075de4c6bbcb89aa4a996f6a9a3f055b58f3d8a3318c7908033120ae6e0c5aa27baba08ff03c32d9c3506cd2e326e91dc621f453ce2f5d  stellarium-0.22.0.tar.gz
"
